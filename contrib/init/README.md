Sample configuration files for:

SystemD: faircoind.service
Upstart: faircoind.conf
OpenRC:  faircoind.openrc
         faircoind.openrcconf
CentOS:  faircoind.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
